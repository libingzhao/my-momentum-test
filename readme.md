## Steps to run in local machine

First 1:
The git repository :https://yeslucky5@bitbucket.org/yeslucky5/my-momentum-test.git

Run:git clone https://yeslucky5@bitbucket.org/yeslucky5/my-momentum-test.git

Step 2:
locate the root folder of the repository, in this case under blog folder, which have readme.md  and composer.json and more.

Run:
composer install
composer update

Step 3:

Run:
php artisan serve

the terminal will shows the local address.
It may be http://127.0.0.1:8000/ at most cases


## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
