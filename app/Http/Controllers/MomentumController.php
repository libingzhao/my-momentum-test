<?php
	
namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\View\View;
use View;
use Illuminate\Support\Facades\Input;

class MomentumController extends Controller
{
	/*
	   "films": "https://swapi.co/api/films/",
    "people": "https://swapi.co/api/people/",
    "planets": "https://swapi.co/api/planets/",
    "species": "https://swapi.co/api/species/",
    "starships": "https://swapi.co/api/starships/",
    "vehicles": "https://swapi.co/api/vehicles/"
	*/
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
     
    public function index(Request $request)
    {
      	 $client = new \GuzzleHttp\Client();
      	 
	  	 $people = $client->request('GET', 'https://swapi.co/api/people/');
         $people_data=  $people->getBody();
         
         $planets = $client->request('GET', 'https://swapi.co/api/planets/');
         $planets_data=  $planets->getBody();

         $starships = $client->request('GET', 'https://swapi.co/api/starships/');
         $starships_data=  $starships->getBody();

         return View::make('index', compact('people_data'))->with('planets_data',   $planets_data)
                                                           ->with('starships_data', $starships_data);
    }
    
    public function planetPages(Request $request,$pageId){
// 	   
	    $client = new \GuzzleHttp\Client();
	    
	    if($pageId<2){
		    $planets = $client->request('GET', 'https://swapi.co/api/planets/?page=1');
	    }else {
		    $planets = $client->request('GET', 'https://swapi.co/api/planets/?page='.$pageId.'');

	    }
	    $planets_data=  $planets->getBody();
	    
	    $people = $client->request('GET', 'https://swapi.co/api/people/');
        $people_data=  $people->getBody();
		
         $starships = $client->request('GET', 'https://swapi.co/api/starships/');
         $starships_data=  $starships->getBody();
         return View::make('index', compact('people_data'))->with('planets_data',   $planets_data)
                                                           ->with('starships_data', $starships_data);

	    
    }
    public function peoplePages(Request $request,$pageId){
	    $client = new \GuzzleHttp\Client();
	    
	    $planets = $client->request('GET', 'https://swapi.co/api/planets/');
	    $planets_data=  $planets->getBody();
	    
	    if($pageId<2){
		 	 $people = $client->request('GET', 'https://swapi.co/api/people/?page='.$pageId.'');
	    }else {
		 	 $people = $client->request('GET', 'https://swapi.co/api/people/?page='.$pageId.'');
   
	    }
        $people_data=  $people->getBody();
		
         $starships = $client->request('GET', 'https://swapi.co/api/starships/');
         $starships_data=  $starships->getBody();
         return View::make('index', compact('people_data'))->with('planets_data',   $planets_data)
                                                           ->with('starships_data', $starships_data);


	
	}
	public function starshipPages(Request $request,$pageId){
		  $client = new \GuzzleHttp\Client();
	    
	    $planets = $client->request('GET', 'https://swapi.co/api/planets/');
	    $planets_data=  $planets->getBody();
	    
	    $people = $client->request('GET', 'https://swapi.co/api/people/');
        $people_data=  $people->getBody();
		
		if($pageId<2){
         	$starships = $client->request('GET', 'https://swapi.co/api/starships/?page=1');
	    }else {
		  	 $starships = $client->request('GET', 'https://swapi.co/api/starships/?page='.$pageId.'');

		 }
         $starships_data=  $starships->getBody();
         return View::make('index', compact('people_data'))->with('planets_data',   $planets_data)
                                                           ->with('starships_data', $starships_data);

	
	}
		public function store(Request $request){
			 $post_all = $request->all();
			 $data = $post_all['searchname'];
			 
			 $client = new \GuzzleHttp\Client();
			 // the default datas
			 
			 $people = $client->request('GET', 'https://swapi.co/api/people/');
			 $people_data=  $people->getBody();
         
			 $planets = $client->request('GET', 'https://swapi.co/api/planets/');
			 $planets_data=  $planets->getBody();

			 $starships = $client->request('GET', 'https://swapi.co/api/starships/');
			 $starships_data=  $starships->getBody();
		 	//  end of the default datas
			 $array_page =array();
			 $array = array('foo' => 'bar');
			 
			 for( $i=1; $i <9; $i++){
			     $people = $client->request('GET', 'https://swapi.co/api/people/?page='.$i.'');
			     
			     if($people->getStatusCode()=='200'){
					$people_data =  $people->getBody();
			        $people_array = json_decode($people_data, true);
					$new_array = $people_array['results'];
					foreach ($new_array as $key => $single_array) {
// 					 print_r( $single_array);
					 if( str_contains($single_array['name'],$data)){
						 $array_page = &$single_array;
						 break;
					 	}
					}
			     }
		        }
		        function array_to_object($array) {
				    return (object) $array;
				}
				
				array_to_object($array_page);
// 				print_r($array_page);
				if(empty($array_page))
				{
					$empty = 'true';
				}
				return View::make('index', compact('people_data'))->with('planets_data',   $planets_data)
                                                           ->with('starships_data', $starships_data)
                                                           ->with(compact('array_page'))
                                                            ->with(compact('empty'));

	}

}

