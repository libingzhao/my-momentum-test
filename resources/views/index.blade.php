<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <title>My Momentum Test</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
			.title{
				text-align:center;
			}
			.active
			{
				display:none ;
			}
			.the-detail{
				    display: inline-grid;
			}
			.toggle{
				background: green;
			    text-align: center;
			    font-size: 1.2em;
			}
			.border{
				border:1px solid green;
			}
			
		</style>
    </head>
    
	<body>
		<div class="flex-center position-ref full-height">
			<div class="content">
				<div class="title m-b-md">
	                    My Test
	                </div>
				<div id="app" class ="container">
					<div>
					  {{Form::open(array
                    (
                    'method'    => 'post', 
                    'url'       => '/',
                    'id'        => 'submition-form',
                    'enctype'   => 'multipart/form-data',
                    )
                    )}}
						Search people by name:<br>
						<input type="text" name="searchname" value="">
						<input type="submit" value="Submit">
					{{Form::close()}}
					
					</div>
					@isset($array_page)
					   <div class="col-sm col-md result border">
						   <span>Search results:</span>
						   @isset($array_page['name'])
							  	<h1>{{ $array_page['name'] }}</h1>
							  	 <details class="the-detail" >
								    <div>Gender:{{ $array_page['gender'] }}</div>
								    <div>Hair_color:{{ $array_page['hair_color'] }}</div>
								    <div>Height:{{ $array_page['height'] }}</div>
								    <div>Skin color:{{$array_page['skin_color'] }}</div>
									<div>Birth year:{{ $array_page['birth_year'] }}</div>
							    </details>
							@endisset	
							</div>
					@endisset
					@isset($empty)
					 not found
					@endisset
					<h2>Plants</h2>
					<div class="row">
						<div v-for="planet in planets.results">
							<div class="col-sm col-md result">
							  	<h1>@{{ planet.name }}</h1>
							  	 <details class="the-detail" >
								    <div>Population:@{{ planet.population }}</div>
								    <div>Gravity:@{{ planet.gravity }}</div>
								    <div>Orbital period:@{{ planet.orbital_period }}</div>
								    <div>Rotation period:@{{ planet.rotation_period }}</div>
								    <div>Surface water:@{{ planet.surface_water }}</div>
								    <div>Terrain:@{{ planet.terrain }}</div>
							    </details>
							</div>
						</div>
					</div>
					<div style="text-align: center">
						<!-- Get the datas for pagination  -->
						<?php 	
							$copy_plants = $planets_data;
							$leads = json_decode($copy_plants, true);
							$previous = $leads['previous'];
							$next = $leads['next'];
							$previous_url = substr($previous, strpos($previous, "=") + 1);
							$next_url = substr($next, strpos($next, "=") + 1);
							$current_page = url()->current();
							
							if(str_contains($current_page, 'planet') ){
								 $plant_number = substr($current_page, strpos($current_page, "et/") + 3);
							 }
							 else {
								  $plant_number ='1';
							 }
						?>
						<a href="/planet/{{$previous_url}}"  class="<?php if($previous==NULL){ echo 'disabled' ;} ?>" >Previous</a>
						Current Page {{$plant_number}}
						<a href="/planet/{{ $next_url}}">Next</a>
					</div>
					<hr>
					<h2>People</h2>
					<div class="row">
						<div  v-for="people in peoples.results">
							<div class="col-sm col-md result">
							  			<h1>@{{ people.name }}</h1>
							  		 <details class="the-detail" >
								    <div>Gender:@{{ people.gender }}</div>
								    <div>Hair_color:@{{ people.hair_color }}</div>
								    <div>Height:@{{ people.height }}</div>
								    <div>Rotationperiod:@{{ people.rotation_period }}</div>
								    <div>Skin color:@{{ people.skin_color }}</div>
									<div>Birth year:@{{ people.birth_year }}</div>
							    </details>
							</div>
						</div>
					</div>
					<div style="text-align: center">
						<?php 
						
							$copy_peoples = $people_data;
							$people_array = json_decode($copy_peoples, true);
							$people_previous = $people_array['previous'];
							$people_next = $people_array['next'];
							$people_previous_url = substr($people_previous, strpos($people_previous, "=") + 1);
							$people_next_url = substr($people_next, strpos($people_next, "=") + 1);
							$people_current_page = url()->current();
							
							if(str_contains($people_current_page, 'people') ){
								 $people_number = substr($people_current_page, strpos($people_current_page, "le/") + 3);
							 }
							 else {
								  $people_number ='1';
							 }

							
							
						?>
						<a  <?php if(!isset($people_previous)){ echo 'disabled' ;} ?> href="/people/{{$people_previous_url}}">Previous</a>
						Current Page {{$people_number}}
						<a href="/people/{{ $people_next_url}}">Next</a>
					</div>
					<hr>
					<h2>Starships</h2>
					<div class="row">
						<div v-for="starship in starships.results">
							<div class="col-sm  col-md ">
							  		<h1>@{{ starship.name }}</h1>
							  		 <details class="the-detail"  v-bind:class="{'active' : detailHidden}">
								    <div>Length:@{{ starship.length }}</div>
								    <div>Cargo capacity:@{{ starship.cargo_capacity }}</div>
								    <div>Cost in credits:@{{ starship.cost_in_credits }}</div>
								    <div>Rotation period:@{{ starship.rotation_period }}</div>
								    <div>Length:@{{ starship.length }}</div>
									<div>Model:@{{ starship.model }}</div>
								   <div>Starship class:@{{ starship. starship_class}}</div>
							    </details>

							  </div>
						</div>
					</div>
					<div style="text-align: center">
							<?php 
						
							$copy_starship = $starships_data;
							$starship_array = json_decode($copy_starship, true);
							$starship_previous = $starship_array['previous'];
							$starship_next = $starship_array['next'];
							$starship_previous_url = substr($starship_previous, strpos($starship_previous, "=") + 1);
							$starship_next_url = substr($starship_next, strpos($starship_next, "=") + 1);
							$starship_current_page = url()->current();
							
							if(str_contains($starship_current_page, 'starship') ){
								 $starship_number = substr($starship_current_page, strpos($starship_current_page, "ip/") + 3);
							 }
							 else {
								  $starship_number ='1';
							 }
						?>
						<a  <?php if(!isset($starship_previous)){ echo 'disabled' ;} ?> href="/starship/{{$starship_previous_url}}">Previous</a>
						Current Page {{$starship_number}}
						<a href="/starship/{{ $starship_next_url}}">Next</a>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function(){
				
			});
			
		    var app = new Vue({
			  el: '#app',
			  data: {
			    planets:  {!! $planets_data !!},
			    starships:  {!! $starships_data !!},
			    peoples:  {!! $people_data !!},
			    detailHidden: true,
			  	},
			  // method 
			  methods: {
			      
			  }
			  // end method
			})
		    </script>
	</body>
</html>
